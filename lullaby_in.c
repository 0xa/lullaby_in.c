/*
 *  ░█░░░█░█░█░░░█░░░█▀█░█▀▄░█░█░░░░░▀█▀░█▀█░░░░█▀▀
 *  ░█░░░█░█░█░░░█░░░█▀█░█▀▄░░█░░░░░░░█░░█░█░░░░█░░
 *  ░▀▀▀░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀▀░░░▀░░▀▀▀░▀▀▀░▀░▀░▀░░▀▀▀
 *
 *
 *  http://0xa.kuri.mu
 *
 */

#include <gba_console.h>
#include <gba_video.h>
#include <gba_sound.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>
#include <gba_input.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


/*
 * Taken from http://belogic.com/gba/files/gba.h
 */

#define REG_WAVE_RAM0       *(vu32*)0x4000090
#define REG_WAVE_RAM1       *(vu32*)0x4000094
#define REG_WAVE_RAM2       *(vu32*)0x4000098
#define REG_WAVE_RAM3       *(vu32*)0x400009C
#define SOUND3BANK32        0x0000
#define SOUND3SETBANK0      0x0000
#define SOUND3SETBANK1      0x0040
#define SOUND3PLAYLOOP      0x0000
#define SOUND3INIT          0x8000
#define SOUND3OUTPUT1       0x2000
#define SOUND3PLAY          0x0080


/*
 * Variables and Arrays
 */

int rate = 32;
int frame = 0;
int step = 0;

int S3freq;

int S1 [6], S2 [6], S3 [6], S4 [6];


int WBANK [2] = {SOUND3SETBANK0, SOUND3SETBANK1};


/*
 * Some heptatonic scales with C as tonic and G as dominant
 *
 * I  bII    II  bIII   III  IV  bV     V  bVI    VI  bVII   VII  I
 * C  C#/Db  D   D#/Eb  E    F   F#/Gb  G  G#/Ab  A   A#/Bb  B    C
 *
 * Minor Gipsy: 1, ½, 1½, ½, ½, 1, 1 -> C, D, D#/Eb, F#/Gb, G, G#/Ab, A#/Bb, C
 */

int S [8] = {0x0416,0x0483,0x04B5,0x053B,0x0563,0x0589,0x05ce,0x060B};
int C [6] = {0x002C, 0x0416, 0x060B, 0x0705, 0x0783, 0x07C1};
int CG [12] = {0x002c,0x02c7,0x0416,0x0563,0x060b,0x06b2,0x0706,0x0759,0x0783,0x07ac,0x07c1,0x07d6};


/*
 * Quick and dirty random number generator taken from cearn (Jasper Vijn)
 * http://www.coranac.com/tonc/text/gfx.htm#cd-qran
 */

int __qran_seed= 0xA;     // Seed / rnd holder (42)

int sqran(int seed)
{	
    int old= __qran_seed;
    __qran_seed= seed; 
    return old;	
}

int qran()
{	
    __qran_seed= 1664525*__qran_seed+1013904223;
    return (__qran_seed>>16) & 0x7FFF;
}

int qran_r(int min, int max)	
{    return (qran()*(max-min)>>15)+min;     }


//////////////////////////////////////////////////////////////////////////////
// Sound generators
//////////////////////////////////////////////////////////////////////////////

void setSnd3(void)
{
    // DMG Channel 3 4-bit DAC Bank parameters
    // See specs: http://belogic.com/gba/channel3.shtml

    // Select BANK1 for playing and write crap to BANK0
    REG_SOUND3CNT_L = SOUND3BANK32 | SOUND3SETBANK1;
    REG_SOUND3CNT_L |= SOUND3PLAY;
    REG_SOUND3CNT_H = 0xFFFF;
    REG_SOUND3CNT_X=SOUND3INIT|SOUND3PLAYLOOP| S3freq ;
    REG_WAVE_RAM0=qran_r(0,65536);

    // Select BANK0 for playing and write crap to BANK1
    REG_SOUND3CNT_L = SOUND3BANK32 | SOUND3SETBANK0;
    REG_SOUND3CNT_L |= SOUND3PLAY;
    REG_SOUND3CNT_H = 0xFFFF;
    REG_SOUND3CNT_X=SOUND3INIT|SOUND3PLAYLOOP| S3freq ;
    REG_WAVE_RAM1=qran_r(0,65536);
}

int setSnd4(int cd, int sta, int stp, int ti, int re)
{
    // DMG Channel 4 Noise parameters, reset and loop control
    // See specs: http://belogic.com/gba/registers.shtml#REG_SOUND4CNT_H
    //
    // cd    -- Clock divider frequency (0-7)
    // sta   -- Counter stages (0-1), 15 or 7
    // stp   -- Counter Pre-Stepper frequency (0-15)
    // ti    -- Timed mode (0-1), continuous or timed
    // re    -- Sound reset

    return (re << 15) | (ti << 14) | (stp << 4) | (sta << 3) | cd;
}


/*
 * Sequence generators
 */

void
newS1(void)
{
    int n;
    for (n=0; n<6; n++) {
        if (n == 0 || n == 3)
            S1[n] = CG[qran_r(0,12)] | 0xC000;
        else
            S1[n] = CG[qran_r(0,12)] | 0xC000;
    }
}

void
newS2(void)
{
    int n;

    for (n=0; n<6; n++) {
        if (n == 0 || n == 3)
            S2[n] = C[qran_r(0,6)] | 0xC000;
        else
            S2[n] = CG[qran_r(0,12)] | 0xC000;
    }
}

void
newS3(void)
{
    int n;
    for (n=0; n<6; n++)
        S3[n] = qran_r(0,2);
}

void
newS4(void)
{
    int n;
    for (n=0; n<6; n++)
        S4[n] = setSnd4(qran_r(0,8),qran_r(0,2),qran_r(0,16),qran_r(0,2),1);
}

void
newS(void)
{

    //    setSnd3();
    newS1();
    newS2();
    newS3();
    newS4();

    // Should not be here but ...

}

//////////////////////////////////////////////////////////////////////////////
// Note triggers
//////////////////////////////////////////////////////////////////////////////

inline int 
playNote1(int freq)
{
    REG_SOUND1CNT_L=freq<<2;
    REG_SOUND1CNT_H=freq;
    REG_SOUND1CNT_X=freq;

    return 0;
}

inline int
playNote2(int freq)
{
	REG_SOUND2CNT_L=freq>>1;
    REG_SOUND2CNT_H=freq;

    return 0;
}

inline int
playNote3(int bank)
{
    REG_SOUND3CNT_L = SOUND3BANK32 | WBANK[bank];
    REG_SOUND3CNT_L |= SOUND3PLAY;
    REG_SOUND3CNT_H = 0xFFFF;
    REG_SOUND3CNT_X=SOUND3INIT|SOUND3PLAYLOOP| S3freq ;

    return 0;
}

inline int
playNote4(int noise)
{
    REG_SOUND4CNT_L=0xC100;
    REG_SOUND4CNT_H=noise;

    return 0;
}

/*
 * Do you even REG_SOUNDCNT_L?
 * 11 -> S1
 * 22 -> S2
 * 33 -> S1+S2
 * 44 -> S3
 * 55 -> S1+S3
 * 66 -> S2+S3
 * 77 -> S1+S2+S3
 * FF -> ALL
 */

inline void
initSound(void)
{
    
    //REG_SOUNDCNT_X = 0x80; //turn on sound circuit
    REG_SOUNDCNT_X = 0x0080;
    REG_SOUNDCNT_L=0x2277;
    // Overall output ratio - Full
    REG_SOUNDCNT_H = 0x0002;


    REG_SOUND3CNT_L |= SOUND3PLAY;
    REG_SOUND3CNT_H = 0xFFFF;
    REG_SOUND3CNT_X=SOUND3INIT|SOUND3PLAYLOOP| 0x060B ; //play a C-4 in loop mode
}   

void
rndMix(void)
{
    REG_SOUNDCNT_L=qran_r(0,65536);
}

inline void
splash(void)
{
	consoleDemoInit();
    
    iprintf("\x1b[2;0H+++++++++++++++++++++++++++++++");
    iprintf("\x1b[3;0H    ______                     ");
    iprintf("\x1b[4;0H   /      |       ________     ");
    iprintf("\x1b[5;0H  /       |____  /        \\    ");
    iprintf("\x1b[6;0H /        |    \\/   _      \\   ");
    iprintf("\x1b[7;0H \\       ___       /_\\      \\  ");
    iprintf("\x1b[8;0H  \\      \\_/          .      \\ ");
    iprintf("\x1b[9;0H   \\          /\\_____/       / ");
    iprintf("\x1b[10;0H    \\________/      |       /  ");
    iprintf("\x1b[11;0H                    |______/   ");
    iprintf("\x1b[13;0H+++++++++++++++++++++++++++++++");

    iprintf("\x1b[17;0H       lullaby_in.c v1");
}


int
main(void)
{

    int keys_pressed;
    int hasPlayedS1 = 0;
    int hasPlayedS2 = 0;
    int hasPlayedS3 = 0;
    int hasPlayedS4 = 0;


   	irqInit();
	irqEnable(IRQ_VBLANK);
    initSound();



    splash();

    newS();

    //setScale();
    setSnd3();


    

	for (;;) {
		

        int step6 = step%6;

        VBlankIntrWait();
        scanKeys();
        keys_pressed = keysDown();
        //keys_released = keysUp();

        if (step%30 == 0 && frame%rate == 0)
        {
            //newS();
            hasPlayedS1 = 0;
            hasPlayedS2 = 0;
            hasPlayedS3 = 0;
            hasPlayedS4 = 0;
        }


        if (frame%rate == 0)
        {
            step++;
            
            hasPlayedS1 = 0;
            hasPlayedS3 = 0;

            BG_COLORS[0]=RGB8(qran_r(0,256),qran_r(0,256),qran_r(0,256));
            
            //consoleCls();
        }



        if (frame%(rate>>1)==0) // cheapo delay
        {
            hasPlayedS2 = 0;
            hasPlayedS4 = 0;
        }

		if (hasPlayedS1 == 0)
        {
            playNote1(S1[step6]);
            hasPlayedS1 = 1;
        }
  
        if (hasPlayedS2 == 0)
        {
            playNote2(S2[step6]);
            hasPlayedS2 = 1;
        }

        if (hasPlayedS3 == 0)
        {
            playNote3(S3[step6]);
            hasPlayedS3 = 1;
        }


        if (S4[step6] != 0 && hasPlayedS4 == 0)
        {
            playNote4(S4[step6]);
            hasPlayedS4 = 1;
            
        }

        frame++;

        if ( keys_pressed & KEY_UP && rate > 1)
            rate--;

        if ( keys_pressed & KEY_DOWN)
            rate++;

        if ( keys_pressed & KEY_A)
            newS();   

        if ( keys_pressed & KEY_B)
        {
            int newfreq = S3freq;
            while (newfreq == S3freq)
                newfreq = S[qran_r(0,8)];
            S3freq = newfreq;
        }

        if ( keys_pressed & KEY_SELECT)
        {
            rndMix();
            setSnd3();
        }

    }
}